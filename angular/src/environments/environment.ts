import { Environment } from '@abp/ng.core';

const baseUrl = 'http://localhost:4200';

export const environment = {
  production: false,
  application: {
    baseUrl,
    name: 'AbpCoding',
  },
  oAuthConfig: {
    issuer: 'https://localhost:44304',
    redirectUri: baseUrl,
    clientId: 'AbpCoding_App',
    responseType: 'code',
    scope: 'offline_access AbpCoding',
  },
  apis: {
    default: {
      url: 'https://localhost:44375',
      rootNamespace: 'Hd.AbpCoding',
    },
  },
} as Environment;
