import { Component } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  template: `
    <app-host-dashboard *abpPermission="'AbpCoding.Dashboard.Host'"></app-host-dashboard>
    <app-tenant-dashboard *abpPermission="'AbpCoding.Dashboard.Tenant'"></app-tenant-dashboard>
  `,
})
export class DashboardComponent {}
