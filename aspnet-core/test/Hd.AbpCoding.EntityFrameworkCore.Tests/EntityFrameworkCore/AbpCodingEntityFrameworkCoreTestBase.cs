﻿using Volo.Abp;

namespace Hd.AbpCoding.EntityFrameworkCore
{
    public abstract class AbpCodingEntityFrameworkCoreTestBase : AbpCodingTestBase<AbpCodingEntityFrameworkCoreTestModule> 
    {

    }
}
