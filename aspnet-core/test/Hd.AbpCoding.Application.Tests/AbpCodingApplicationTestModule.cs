﻿using Volo.Abp.Modularity;

namespace Hd.AbpCoding
{
    [DependsOn(
        typeof(AbpCodingApplicationModule),
        typeof(AbpCodingDomainTestModule)
        )]
    public class AbpCodingApplicationTestModule : AbpModule
    {

    }
}