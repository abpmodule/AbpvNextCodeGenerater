﻿using Hd.AbpCoding.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Hd.AbpCoding
{
    [DependsOn(
        typeof(AbpCodingEntityFrameworkCoreTestModule)
        )]
    public class AbpCodingDomainTestModule : AbpModule
    {

    }
}