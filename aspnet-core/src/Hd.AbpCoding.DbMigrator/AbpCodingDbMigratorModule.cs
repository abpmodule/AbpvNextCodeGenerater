﻿using Hd.AbpCoding.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace Hd.AbpCoding.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(AbpCodingEntityFrameworkCoreDbMigrationsModule),
        typeof(AbpCodingApplicationContractsModule)
    )]
    public class AbpCodingDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options =>
            {
                options.IsJobExecutionEnabled = false;
            });
        }
    }
}