﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hd.AbpCoding.Projects.Dtos
{
    public class CreateProjectDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string RootNamespace { get; set; }
    }
}
