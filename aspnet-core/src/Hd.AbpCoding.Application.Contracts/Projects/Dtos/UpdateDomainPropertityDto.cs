﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hd.AbpCoding.Projects.Dtos
{
    public class UpdateDomainPropertityDto
    {
        public Guid DomainId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public bool IsRequired { get; set; }

        public string PropertityType { get; set; }

        public int MaxLength { get; set; }

        /// <summary>
        /// 是否创建索引
        /// </summary>
        public bool Index { get; set; }

        /// <summary>
        /// set 方法的修饰符
        /// </summary>
        public PropertityQualifier SetQualifier { get; set; }

        public PropertityQualifier Qualifier { get; set; }

        /// <summary>
        /// 排序（生成构造的时候的参数顺序）
        /// </summary>
        public int Sort { get; set; }

        public DomainPropertityType DomainPropertityType { get; set; }
    }
}
