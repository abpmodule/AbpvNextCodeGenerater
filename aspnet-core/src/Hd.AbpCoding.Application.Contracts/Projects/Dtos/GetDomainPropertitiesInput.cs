﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Hd.AbpCoding.Projects.Dtos
{
    public class GetDomainPropertitiesInput : PagedAndSortedResultRequestDto
    {
        public Guid DomainId { get; set; }
    }
}
