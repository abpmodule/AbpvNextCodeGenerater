﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Hd.AbpCoding.Projects.Dtos
{
    public class GetProjectsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
