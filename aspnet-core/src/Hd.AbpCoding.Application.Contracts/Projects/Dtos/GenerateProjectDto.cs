﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hd.AbpCoding.Projects.Dtos
{
    public class GenerateProjectDto
    {
        public Guid ProjectId { get; set; }
    }
}
