﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hd.AbpCoding.Projects.Dtos
{
    public class GenerateDomainDto
    {
        public Guid ProjectId { get; set; }

        public Guid DomainId { get; set; }
    }
}
