﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Hd.AbpCoding.Projects.Dtos
{
    public class ProjectDto : FullAuditedEntityDto<Guid>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string RootNamespace { get; set; }

    }
}
