﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Hd.AbpCoding.Projects.Dtos
{
    public class GetDomainDescsInput: PagedAndSortedResultRequestDto
    {
        public Guid ProjectId { get; set; }

        public string Filter { get; set; }
    }
}
