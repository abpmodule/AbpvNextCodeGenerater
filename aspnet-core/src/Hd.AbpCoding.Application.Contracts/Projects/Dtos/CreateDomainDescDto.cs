﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hd.AbpCoding.Projects.Dtos
{
    public class CreateDomainDescDto
    {
        public Guid ProjectId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 是否是聚合根
        /// </summary>
        public bool IsAggregateRoot { get; set; }

        public AuditTypes AuditInterfaceType { get; set; }

        /// <summary>
        /// 是否添加审计日志
        /// </summary>
        public bool Audited { get; set; }

        public bool EnableMultiTenant { get; set; }

        public PrimaryKeyTypes PrimaryKeyType { get; set; }
    }
}
