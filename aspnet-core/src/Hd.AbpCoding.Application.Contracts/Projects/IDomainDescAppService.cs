﻿using Hd.AbpCoding.Projects.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Services;

namespace Hd.AbpCoding.Projects
{
    public interface IDomainDescAppService : ICrudAppService<DomainDescDto, Guid, GetDomainDescsInput, CreateDomainDescDto, UpdateDomainDescDto>, IApplicationService
    {
    }
}
