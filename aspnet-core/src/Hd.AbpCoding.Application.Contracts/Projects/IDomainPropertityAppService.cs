﻿using Hd.AbpCoding.Projects.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Services;

namespace Hd.AbpCoding.Projects
{
    public interface IDomainPropertityAppService : ICrudAppService<DomainPropertityDto, Guid, GetDomainPropertitiesInput, CreateDomainPropertityDto, UpdateDomainPropertityDto>, IApplicationService
    {
    }
}
