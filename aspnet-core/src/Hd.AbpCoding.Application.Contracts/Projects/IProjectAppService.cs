﻿using Hd.AbpCoding.Projects.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Hd.AbpCoding.Projects
{

    public interface IProjectAppService : ICrudAppService<ProjectDto, Guid, GetProjectsInput, CreateProjectDto, UpdateProjectDto>, IApplicationService
    {

        Task GenerateDomainAsync(GenerateDomainDto input);

        Task GenerateProjectAsync(GenerateProjectDto input);
    }
}
