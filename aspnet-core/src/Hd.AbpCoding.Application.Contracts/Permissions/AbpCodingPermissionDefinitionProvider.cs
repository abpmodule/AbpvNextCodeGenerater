using Hd.AbpCoding.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Abp.MultiTenancy;

namespace Hd.AbpCoding.Permissions
{
    public class AbpCodingPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(AbpCodingPermissions.GroupName);

            myGroup.AddPermission(AbpCodingPermissions.Dashboard.Host, L("Permission:Dashboard"), MultiTenancySides.Host);
            myGroup.AddPermission(AbpCodingPermissions.Dashboard.Tenant, L("Permission:Dashboard"), MultiTenancySides.Tenant);

            var project = myGroup.AddPermission(AbpCodingPermissions.Project.Default, L("Permission:Project"), MultiTenancySides.Host);
            project.AddChild(AbpCodingPermissions.Project.Create, L("Permission:Create"), MultiTenancySides.Host);
            project.AddChild(AbpCodingPermissions.Project.Delete, L("Permission:Delete"), MultiTenancySides.Host);
            project.AddChild(AbpCodingPermissions.Project.Update, L("Permission:Update"), MultiTenancySides.Host);

            var domain = myGroup.AddPermission(AbpCodingPermissions.DomainDesc.Default, L("Permission:DomainDesc"), MultiTenancySides.Host);
            domain.AddChild(AbpCodingPermissions.Project.Create, L("Permission:Create"), MultiTenancySides.Host);
            domain.AddChild(AbpCodingPermissions.Project.Delete, L("Permission:Delete"), MultiTenancySides.Host);
            domain.AddChild(AbpCodingPermissions.Project.Update, L("Permission:Update"), MultiTenancySides.Host);

            var domainPropertity = myGroup.AddPermission(AbpCodingPermissions.DomainPropertity.Default, L("Permission:DomainPropertity"), MultiTenancySides.Host);
            domainPropertity.AddChild(AbpCodingPermissions.Project.Create, L("Permission:Create"), MultiTenancySides.Host);
            domainPropertity.AddChild(AbpCodingPermissions.Project.Delete, L("Permission:Delete"), MultiTenancySides.Host);
            domainPropertity.AddChild(AbpCodingPermissions.Project.Update, L("Permission:Update"), MultiTenancySides.Host);
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<AbpCodingResource>(name);
        }
    }
}