namespace Hd.AbpCoding.Permissions
{
    public static class AbpCodingPermissions
    {
        public const string GroupName = "AbpCoding";

        public static class Dashboard
        {
            public const string DashboardGroup = GroupName + ".Dashboard";
            public const string Host = DashboardGroup + ".Host";
            public const string Tenant = DashboardGroup + ".Tenant";
        }

        public static class Project
        {
            public const string Default = GroupName + ".Project";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
            public const string Update = Default + ".Update";

        }

        public static class DomainDesc
        {
            public const string Default = GroupName + ".DomainDesc";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
            public const string Update = Default + ".Update";
        }

        public static class DomainPropertity
        {
            public const string Default = GroupName + ".DomainPropertity";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
            public const string Update = Default + ".Update";
        }

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";
    }
}
