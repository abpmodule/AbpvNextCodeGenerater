﻿using Volo.Abp.Localization;

namespace Hd.AbpCoding.Localization
{
    [LocalizationResourceName("AbpCoding")]
    public class AbpCodingResource
    {

    }
}