﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hd.AbpCoding.Projects
{
    public enum PrimaryKeyTypes : byte
    {
        Guid = 0,
        Int = 1,
        字符串 = 2,
        复合主键 = 3
    }
}
