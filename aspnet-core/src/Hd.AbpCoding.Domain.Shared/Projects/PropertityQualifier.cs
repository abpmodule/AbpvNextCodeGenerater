﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hd.AbpCoding.Projects
{
    public enum PropertityQualifier : byte
    {
        私有的 = 0,
        公共的 = 1,
        受保护的 = 2
    }
}
