﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hd.AbpCoding.Projects
{
    public static class DomainDescConsts
    {
        public const int MaxNameLength = 128;
        public const int MaxDescriptionLength = 1024;
    }
}
