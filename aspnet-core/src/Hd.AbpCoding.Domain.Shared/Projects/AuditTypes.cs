﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hd.AbpCoding.Projects
{
    public enum AuditTypes
    {
        None = 0,
        Audited = 1,
        FullAudited = 2
    }
}
