﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hd.AbpCoding.Projects
{
    public static class ProjectConsts
    {
        public const int MaxNameLength = 512;
        public const int MaxDescriptionLength = 2048;

    }
}
