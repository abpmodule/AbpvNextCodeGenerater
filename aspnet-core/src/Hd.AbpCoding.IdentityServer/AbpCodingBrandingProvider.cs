﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace Hd.AbpCoding
{
    [Dependency(ReplaceServices = true)]
    public class AbpCodingBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "AbpCoding";
    }
}
