﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Hd.AbpCoding.Projects
{
    public interface IProjectRepository : IBasicRepository<Project, Guid>
    {
        Task<Project> FindByNameAsync(string name, CancellationToken cancellationToken = default);

        Task<int> GetCountAsync(string filter, CancellationToken cancellationToken = default);

        Task<List<Project>> GetListAsync(
            string filter,
            int skipCount = 0,
            int maxResultCount = int.MaxValue,
            string sorting = null,
            bool includeDetails = false,
            CancellationToken cancellationToken = default);
    }
}
