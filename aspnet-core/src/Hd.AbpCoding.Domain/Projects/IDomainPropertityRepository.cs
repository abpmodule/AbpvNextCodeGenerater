﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Hd.AbpCoding.Projects
{
    public interface IDomainPropertityRepository : IBasicRepository<DomainPropertity, Guid>
    {
        Task<DomainPropertity> FindByNameAsync(Guid domainId, string name, CancellationToken cancellationToken = default);

        Task<int> GetCountAsync(Guid domainId, CancellationToken cancellationToken = default);

        Task<List<DomainPropertity>> GetListAsync(
            Guid domainId,
            int skipCount = 0,
            int maxResultCount = int.MaxValue,
            string sorting = null,
            bool includeDetails = false,
            CancellationToken cancellationToken = default);
    }
}
