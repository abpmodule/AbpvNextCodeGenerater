﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;

namespace Hd.AbpCoding.Projects
{
    public class DomainDesc : FullAuditedAggregateRoot<Guid>
    {

        private DomainDesc()
        {

        }

        public DomainDesc(
            Guid id,
            Guid projectId,
            string name,
            PrimaryKeyTypes primaryKeyType = PrimaryKeyTypes.Guid,
            bool isAggregateRoot = true,
            bool enableMultiTenant = false,
            string path = null,
            string description = null,
            bool audited = false
            )
        {
            Id = id;
            ProjectId = projectId;
            Name = name;
            PrimaryKeyType = primaryKeyType;
            IsAggregateRoot = isAggregateRoot;
            EnableMultiTenant = enableMultiTenant;
            Path = path;
            Description = description;
            Audited = audited;

        }

        public virtual DomainDesc Update(
            Guid projectId,
            string name,
            PrimaryKeyTypes primaryKeyType,
            bool isAggregateRoot,
            bool enableMultiTenant,
            string path,
            string description,
            bool audited)
        {
            if (ProjectId == projectId)
            {
                Name = name;
                PrimaryKeyType = primaryKeyType;
                IsAggregateRoot = isAggregateRoot;
                EnableMultiTenant = enableMultiTenant;
                Path = path;
                Description = description;
                Audited = audited;
            }
            return this;
        }

        public virtual Guid ProjectId { get; private set; }

        /// <summary>
        /// 类名
        /// </summary>
        [Required]
        [MaxLength(DomainDescConsts.MaxNameLength)]
        public virtual string Name { get; private set; }

        /// <summary>
        /// 注释
        /// </summary>
        [MaxLength(DomainDescConsts.MaxDescriptionLength)]
        public virtual string Description { get; set; }

        /// <summary>
        /// 路径
        /// </summary>
        public virtual string Path { get; set; }

        /// <summary>
        /// 是否是聚合根
        /// </summary>
        public virtual bool IsAggregateRoot { get; private set; }

        public virtual AuditTypes AuditInterfaceType { get; private set; }

        /// <summary>
        /// 是否添加审计日志
        /// </summary>
        public virtual bool Audited { get; private set; }

        public virtual bool EnableMultiTenant { get; private set; }

        public virtual PrimaryKeyTypes PrimaryKeyType { get; set; }


    }
}
