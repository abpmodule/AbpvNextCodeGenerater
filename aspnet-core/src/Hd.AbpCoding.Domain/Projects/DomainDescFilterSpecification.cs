﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Specifications;

namespace Hd.AbpCoding.Projects
{
    public class DomainDescFilterSpecification : Specification<DomainDesc>
    {

        public string Filter { get; set; }
        public DomainDescFilterSpecification(string filter)
        {
            Filter = filter;
        }

        public override Expression<Func<DomainDesc, bool>> ToExpression()
        {
            return x => x.Name.Contains(Filter) ||
            (!string.IsNullOrEmpty(x.Path) && x.Path.Contains(Filter)) ||
            (!string.IsNullOrEmpty(x.Description) && x.Description.Contains(Filter))
             ;
        }
    }
}
