﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;

namespace Hd.AbpCoding.Projects
{
    public class DomainPropertity : AggregateRoot<Guid>
    {

        public virtual Guid DomainId { get; private set; }

        [Required]
        [MaxLength(DomainPropertityConsts.MaxNameLength)]
        public virtual string Name { get; set; }

        [MaxLength(DomainPropertityConsts.MaxDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual bool IsRequired { get; set; }

        public virtual string PropertityType { get; set; }

        public virtual int MaxLength { get; set; }

        /// <summary>
        /// 是否创建索引
        /// </summary>
        public virtual bool Index { get; set; }

        /// <summary>
        /// set 方法的修饰符
        /// </summary>
        public virtual PropertityQualifier SetQualifier { get; set; }

        public virtual PropertityQualifier Qualifier { get; set; }

        /// <summary>
        /// 排序（生成构造的时候的参数顺序）
        /// </summary>
        public virtual int Sort { get; set; }

        public virtual DomainPropertityType DomainPropertityType { get; set; }

        private DomainPropertity() { }
        public DomainPropertity(
            Guid id,
            Guid domainId,
            string name,
            string type,
            DomainPropertityType domainPropertityType,
            bool isRequired = false,
            bool index = false,
            PropertityQualifier setQualifier = PropertityQualifier.私有的,
            int sort = 1,
            string description = null,
            PropertityQualifier qualifier = PropertityQualifier.公共的
            )
        {
            Id = id;
            DomainId = domainId;
            Name = name;
            PropertityType = type;
            DomainPropertityType = domainPropertityType;
            Index = index;
            IsRequired = isRequired;
            Description = description;
            Qualifier = qualifier;
            SetQualifier = setQualifier;
            Sort = sort;
        }

        public virtual DomainPropertity Update(
            Guid domainId,
            string name,
            string type,
            DomainPropertityType domainPropertityType,
            bool isRequired,
            bool index,
            PropertityQualifier setQualifier,
            int sort,
            string description,
            PropertityQualifier qualifier)
        {
            if (domainId == DomainId)
            {
                Name = name;
                PropertityType = type;
                DomainPropertityType = domainPropertityType;
                Index = index;
                IsRequired = isRequired;
                Description = description;
                Qualifier = qualifier;
                SetQualifier = setQualifier;
                Sort = sort;
            }
            return this;
        }
    }
}
