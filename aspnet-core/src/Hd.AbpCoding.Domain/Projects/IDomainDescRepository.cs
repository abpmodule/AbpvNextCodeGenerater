﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Hd.AbpCoding.Projects
{
    public interface IDomainDescRepository : IBasicRepository<DomainDesc, Guid>
    {

        Task<DomainDesc> FindByNameAsync(Guid projectId, string name, CancellationToken cancellationToken = default);

        Task<List<DomainDesc>> GetListAsync(
            Guid projectId,
            string filter = null,
            int skipCount = 0,
            int maxResultCount = int.MaxValue,
            string sorting = null,
            bool includeDetails = false,
            CancellationToken cancellationToken = default);

        Task<int> GetCountAsync(
            Guid projectId,
            string filter = null,
            CancellationToken cancellationToken = default);

        Task<List<DomainDesc>> GetListAllAsync(Guid projectId, CancellationToken cancellationToken = default);
    }
}
