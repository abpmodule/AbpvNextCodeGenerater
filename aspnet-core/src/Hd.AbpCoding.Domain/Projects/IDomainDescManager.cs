﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace Hd.AbpCoding.Projects
{
    public interface IDomainDescManager : IDomainService
    {
        Task<bool> CheckNameAsync(Guid projectId, string name, Guid? excludeId = null);

        /// <summary>
        /// 生成单个实体的代码
        /// </summary>
        /// <param name="domainId"></param>
        /// <param name="outputFolder"></param>
        /// <returns></returns>
        Task GenerateDomainAsync(Guid domainId, string outputFolder);
    }
}
