﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace Hd.AbpCoding.Projects
{
    public class DomainDescManager : DomainService, IDomainDescManager
    {

        protected IDomainDescRepository Repository { get; }
        private readonly IDomainPropertityRepository _domainPropertityRepository;

        public DomainDescManager(
            IDomainDescRepository domainDescRepository,
            IDomainPropertityRepository domainPropertityRepository)
        {
            Repository = domainDescRepository;
            _domainPropertityRepository = domainPropertityRepository;
        }

        public async Task<bool> CheckNameAsync(Guid projectId, string name, Guid? excludeId = null)
        {
            var domain = await Repository.FindByNameAsync(projectId, name);
            if (excludeId.HasValue)
            {
                return domain == null || domain.Id == excludeId.Value;
            }

            return domain == null;
        }


        public async Task GenerateDomainAsync(Guid domainId, string outputFolder)
        {

        }
    }
}
