﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace Hd.AbpCoding.Projects
{
    public class Project : FullAuditedAggregateRoot<Guid>
    {
        private Project() { }

        public Project(Guid id,
            string name,
            string rootNamespace,
            string description = null)
        {
            Id = id;
            Name = name;
            RootNamespace = rootNamespace;
            Description = description;
        }

        [Required]
        [MaxLength(ProjectConsts.MaxNameLength)]
        public virtual string Name { get; set; }

        [MaxLength(ProjectConsts.MaxDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual string RootNamespace { get; set; }

        

        
    }
}
