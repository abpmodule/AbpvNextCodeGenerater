﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace Hd.AbpCoding.Projects
{
    public interface IProjectManager : IDomainService
    {
        Task<bool> CheckNameAsync(string name, Guid? excludeId = null);

        /// <summary>
        /// 生成整个项目的代码
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        Task GenerateProjectAsync(Guid projectId);

        

        Task<string> GetOutputFolderOrDefaultAsync(Guid projectId);
    }
}
