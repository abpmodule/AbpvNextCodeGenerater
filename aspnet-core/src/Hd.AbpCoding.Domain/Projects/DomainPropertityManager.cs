﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace Hd.AbpCoding.Projects
{
    public class DomainPropertityManager : DomainService, IDomainPropertityManager
    {

        protected IDomainPropertityRepository Repository { get; }
        public DomainPropertityManager(
            IDomainPropertityRepository domainPropertityRepository)
        {
            Repository = domainPropertityRepository;
        }

        public async Task<bool> CheckNameAsync(Guid domainId, string name, Guid? excludeId = null)
        {
            var propertity = await Repository.FindByNameAsync(domainId, name);
            if (excludeId.HasValue)
            {
                return propertity == null || propertity.Id == excludeId.Value;
            }
            return propertity == null;
        }
    }
}
