﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace Hd.AbpCoding.Projects
{
    public interface IDomainPropertityManager : IDomainService
    {
        Task<bool> CheckNameAsync(Guid domainId, string name, Guid? excludeId = null);
    }
}
