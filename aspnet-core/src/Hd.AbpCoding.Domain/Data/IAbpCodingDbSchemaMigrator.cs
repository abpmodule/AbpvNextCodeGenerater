﻿using System.Threading.Tasks;

namespace Hd.AbpCoding.Data
{
    public interface IAbpCodingDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}