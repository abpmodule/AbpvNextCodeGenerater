﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace Hd.AbpCoding.Data
{
    /* This is used if database provider does't define
     * IAbpCodingDbSchemaMigrator implementation.
     */
    public class NullAbpCodingDbSchemaMigrator : IAbpCodingDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}