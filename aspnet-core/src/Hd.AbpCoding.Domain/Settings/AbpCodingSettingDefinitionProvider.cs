﻿using Volo.Abp.Settings;

namespace Hd.AbpCoding.Settings
{
    public class AbpCodingSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(AbpCodingSettings.MySetting1));
        }
    }
}
