﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace Hd.AbpCoding.EntityFrameworkCore
{
    [DependsOn(
        typeof(AbpCodingEntityFrameworkCoreModule)
    )]
    public class AbpCodingEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<AbpCodingMigrationsDbContext>();
        }
    }
}