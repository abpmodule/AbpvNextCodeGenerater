﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Hd.AbpCoding.Data;
using Volo.Abp.DependencyInjection;

namespace Hd.AbpCoding.EntityFrameworkCore
{
    public class EntityFrameworkCoreAbpCodingDbSchemaMigrator
        : IAbpCodingDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreAbpCodingDbSchemaMigrator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the AbpCodingMigrationsDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<AbpCodingMigrationsDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}