﻿using Hd.AbpCoding.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace Hd.AbpCoding.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class AbpCodingController : AbpController
    {
        protected AbpCodingController()
        {
            LocalizationResource = typeof(AbpCodingResource);
        }
    }
}