using Microsoft.EntityFrameworkCore;
using Hd.AbpCoding.Users;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.Modeling;
using Volo.Abp.Identity;
using Volo.Abp.Users.EntityFrameworkCore;
using Hd.AbpCoding.Projects;

namespace Hd.AbpCoding.EntityFrameworkCore
{
    /* This is your actual DbContext used on runtime.
     * It includes only your entities.
     * It does not include entities of the used modules, because each module has already
     * its own DbContext class. If you want to share some database tables with the used modules,
     * just create a structure like done for AppUser.
     *
     * Don't use this DbContext for database migrations since it does not contain tables of the
     * used modules (as explained above). See AbpCodingMigrationsDbContext for migrations.
     */
    [ConnectionStringName("Default")]
    public class AbpCodingDbContext : AbpDbContext<AbpCodingDbContext>
    {
        public DbSet<AppUser> Users { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<DomainDesc> Domains { get; set; }

        public DbSet<DomainPropertity> DomainPropertities { get; set; }

        /* Add DbSet properties for your Aggregate Roots / Entities here.
         * Also map them inside AbpCodingDbContextModelCreatingExtensions.ConfigureAbpCoding
         */

        public AbpCodingDbContext(DbContextOptions<AbpCodingDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            /* Configure the shared tables (with included modules) here */

            builder.Entity<AppUser>(b =>
            {
                b.ToTable(AbpIdentityDbProperties.DbTablePrefix + "Users"); //Sharing the same table "AbpUsers" with the IdentityUser

                b.ConfigureByConvention();
                b.ConfigureAbpUser();

                /* Configure mappings for your additional properties.
                 * Also see the AbpCodingEfCoreEntityExtensionMappings class.
                 */
            });

            /* Configure your own tables/entities inside the ConfigureAbpCoding method */

            builder.ConfigureAbpCoding();
        }
    }
}
