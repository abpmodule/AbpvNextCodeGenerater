using Hd.AbpCoding.Projects;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Hd.AbpCoding.EntityFrameworkCore
{
    public static class AbpCodingDbContextModelCreatingExtensions
    {
        public static void ConfigureAbpCoding(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            builder.Entity<Project>(b =>
            {
                b.ToTable(AbpCodingConsts.DbTablePrefix + "Projects", AbpCodingConsts.DbSchema);
                b.ConfigureByConvention();

                b.Property(x => x.Name).HasColumnName(nameof(Project.Name)).HasMaxLength(ProjectConsts.MaxNameLength).IsRequired();
                b.Property(x => x.Description).HasColumnName(nameof(Project.Description)).HasMaxLength(ProjectConsts.MaxDescriptionLength);
                b.Property(x => x.RootNamespace).HasColumnName(nameof(Project.RootNamespace));

                b.HasIndex(x => x.CreationTime);
                b.HasIndex(x => x.Name);
            });

            builder.Entity<DomainDesc>(b =>
            {
                b.ToTable(AbpCodingConsts.DbTablePrefix + "DomainDescs", AbpCodingConsts.DbSchema);
                b.ConfigureByConvention();

                b.Property(x => x.Name).HasColumnName(nameof(DomainDesc.Name)).HasMaxLength(DomainDescConsts.MaxNameLength).IsRequired();
                b.Property(x => x.Description).HasColumnName(nameof(DomainDesc.Description)).HasMaxLength(DomainDescConsts.MaxDescriptionLength);

                b.HasOne<Project>().WithMany().HasForeignKey(x => x.ProjectId).IsRequired();

                b.HasIndex(x => x.CreationTime);
                b.HasIndex(x => x.Name);
            });

            builder.Entity<DomainPropertity>(b =>
            {
                b.ToTable(AbpCodingConsts.DbTablePrefix + "DomainPropertities", AbpCodingConsts.DbSchema);
                b.ConfigureByConvention();

                b.Property(x => x.Name).HasColumnName(nameof(DomainPropertity.Name)).HasMaxLength(DomainPropertityConsts.MaxNameLength).IsRequired();
                b.Property(x => x.Description).HasColumnName(nameof(DomainPropertity.Description)).HasMaxLength(DomainPropertityConsts.MaxDescriptionLength);
                b.Property(x => x.IsRequired).HasColumnName(nameof(DomainPropertity.IsRequired));
                b.Property(x => x.PropertityType).HasColumnName(nameof(DomainPropertity.PropertityType));
                b.Property(x => x.DomainPropertityType).HasColumnName(nameof(DomainPropertity.DomainPropertityType));
                b.Property(x => x.MaxLength).HasColumnName(nameof(DomainPropertity.MaxLength));
                b.Property(x => x.Index).HasColumnName(nameof(DomainPropertity.Index));
                b.Property(x => x.SetQualifier).HasColumnName(nameof(DomainPropertity.SetQualifier));
                b.Property(x => x.Qualifier).HasColumnName(nameof(DomainPropertity.Qualifier));
                b.Property(x => x.Sort).HasColumnName(nameof(DomainPropertity.Sort));

                b.HasOne<DomainDesc>().WithMany().HasForeignKey(x => x.DomainId);

                b.HasIndex(x => x.Name);
            });

        }
    }
}