﻿using Hd.AbpCoding.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Hd.AbpCoding.Projects
{
    public class DomainPropertityEfCoreRepository : EfCoreRepository<AbpCodingDbContext, DomainPropertity, Guid>, IDomainPropertityRepository
    {
        public DomainPropertityEfCoreRepository(IDbContextProvider<AbpCodingDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<DomainPropertity> FindByNameAsync(Guid domainId, string name, CancellationToken cancellationToken = default)
        {
            return await this.Where(x => x.DomainId == domainId && x.Name == name)
                .FirstOrDefaultAsync(GetCancellationToken(cancellationToken));
        }

        public async Task<int> GetCountAsync(Guid domainId, CancellationToken cancellationToken = default)
        {
            return await this.Where(x => x.DomainId == domainId)
                .CountAsync(GetCancellationToken(cancellationToken));
        }

        public async Task<List<DomainPropertity>> GetListAsync(Guid domainId, int skipCount = 0, int maxResultCount = int.MaxValue, string sorting = null, bool includeDetails = false, CancellationToken cancellationToken = default)
        {
            return await this.Where(x => x.DomainId == domainId)
                .OrderBy(sorting ?? nameof(DomainPropertity.Name))
                .PageBy(skipCount, maxResultCount)
                 .ToListAsync(GetCancellationToken(cancellationToken));
        }
    }
}
