﻿using Hd.AbpCoding.Localization;
using Volo.Abp.Application.Services;

namespace Hd.AbpCoding
{
    /* Inherit your application services from this class.
     */
    public abstract class AbpCodingAppService : ApplicationService
    {
        protected AbpCodingAppService()
        {
            LocalizationResource = typeof(AbpCodingResource);
        }
    }
}
