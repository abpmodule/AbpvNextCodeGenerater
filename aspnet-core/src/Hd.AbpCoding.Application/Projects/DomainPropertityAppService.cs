﻿using Hd.AbpCoding.Permissions;
using Hd.AbpCoding.Projects.Dtos;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Hd.AbpCoding.Projects
{

    [Authorize(AbpCodingPermissions.DomainPropertity.Default)]
    public class DomainPropertityAppService : AbpCodingAppService, IDomainPropertityAppService
    {

        protected IDomainPropertityManager DomainPropertityManager { get; }
        protected IDomainPropertityRepository Repository { get; }

        public DomainPropertityAppService(
            IDomainPropertityManager domainPropertityManager,
            IDomainPropertityRepository domainPropertityRepository)
        {
            DomainPropertityManager = domainPropertityManager;
            Repository = domainPropertityRepository;
        }

        [Authorize(AbpCodingPermissions.DomainPropertity.Create)]
        public async Task<DomainPropertityDto> CreateAsync(CreateDomainPropertityDto input)
        {
            if (!await DomainPropertityManager.CheckNameAsync(input.DomainId, input.Name))
            {
                throw new UserFriendlyException(L["NameAlreadyExisted", input.Name]);
            }

            var propertity = new DomainPropertity(
                GuidGenerator.Create(),
                input.DomainId,
                input.Name,
                input.PropertityType,
                input.DomainPropertityType,
                input.IsRequired,
                input.Index,
                input.SetQualifier,
                input.Sort,
                input.Description,
                input.Qualifier);
            propertity = await Repository.InsertAsync(propertity);
            return MapDto(propertity);
        }

        private DomainPropertityDto MapDto(DomainPropertity propertity)
        {
            return ObjectMapper.Map<DomainPropertity, DomainPropertityDto>(propertity);
        }

        [Authorize(AbpCodingPermissions.DomainPropertity.Delete)]
        public async Task DeleteAsync(Guid id)
        {
            await Repository.DeleteAsync(id);
        }

        public async Task<DomainPropertityDto> GetAsync(Guid id)
        {
            var propertity = await Repository.GetAsync(id);
            return MapDto(propertity);
        }


        public async Task<PagedResultDto<DomainPropertityDto>> GetListAsync(GetDomainPropertitiesInput input)
        {
            var count = await Repository.GetCountAsync(input.DomainId);
            var list = await Repository.GetListAsync(input.DomainId, input.SkipCount, input.MaxResultCount, input.Sorting);
            return new PagedResultDto<DomainPropertityDto>(
                count,
                list.Select(x => MapDto(x)).ToList()
                );
        }

        [Authorize(AbpCodingPermissions.DomainPropertity.Update)]
        public async Task<DomainPropertityDto> UpdateAsync(Guid id, UpdateDomainPropertityDto input)
        {
            var propertity = await Repository.GetAsync(id);
            return MapDto(propertity);
        }
    }
}
