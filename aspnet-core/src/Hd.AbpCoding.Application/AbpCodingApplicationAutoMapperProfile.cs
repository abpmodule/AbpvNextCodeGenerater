using AutoMapper;
using Hd.AbpCoding.Projects;
using Hd.AbpCoding.Projects.Dtos;
using Hd.AbpCoding.Users;
using Volo.Abp.AutoMapper;

namespace Hd.AbpCoding
{
    public class AbpCodingApplicationAutoMapperProfile : Profile
    {
        public AbpCodingApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */

            CreateMap<AppUser, AppUserDto>().Ignore(x => x.ExtraProperties);
            CreateMap<Project, ProjectDto>();
            CreateMap<DomainDesc, DomainDescDto>();
        }
    }
}